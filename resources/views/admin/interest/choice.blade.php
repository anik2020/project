@extends('layout.admin.admin') @section('title','Choice Teacher') @section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Choice Teacher</h2>
            </div>
        </div>

        <!-- Content Header (Page header) -->
        <section class="content-header">
            @if(Session::has('success'))
                <p class="alert alert-success" id="message">{{ Session::get('success') }}</p>
            @endif @if(Session::has('error'))
                <p class="alert alert-warning" id="message">{{ Session::get('error') }}</p>
            @endif

            <div class="container-fluid">
                <div class="row mb-1">
                    <div class="col-sm-7">

                    </div>
                    <div class="col-sm-5">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}"
                                                           style="color: #000 !important;">Home</a></li>
                            <li class="breadcrumb-item">Choice Teacher</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="col-md-8">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Choice Teacher</h3>
                        <small>Select maximum three teachers as supervisor. Then you will get a supervisor to give
                            guidance</small>
                    </div>

                    <form role="form" action="{{ url('student/interest/choice') }}" method="post">
                        @method('POST') @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Select Semester</label>
                                <select name="semester_id" class="form-control ">
                                    @foreach($semesters  as $semester)
                                        <option value="{{ $semester->id }}">{{ $semester->semester_name }}</option>

                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Enter Students Name</label>
                                <input class="form-control" name="students_name">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Enter Students ID</label>
                                <input class="form-control" name="students_id">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Area of Research</label>
                                <input class="form-control" name="area_of_research">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tentative Title</label>
                                <input class="form-control" name="tentative_title">
                            </div>


                            <div class="form-group">
                                <label for="exampleInputEmail1">Select Teacher</label>
                                <select name="teacher_id[]" class="form-control js-example-basic-multiple"
                                        multiple="multiple">
                                    @foreach($teachers as $teacher)
                                        <option value="{{ $teacher->id }}">{{ $teacher->initial }}</option>
                                    @endforeach
                                </select>
                            </div>

                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
                <!-- Form Element sizes --
            </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->


            <!-- /.card -->

    </div>
    </section>
    </div>

    @push('extra-scripts')
        <script src="{{ asset('asset/back/vendor/jquery/jquery.min.js') }}"></script>
        <script>
            $(document).ready(function(){
                alert('hi');
            });
        </script>
    @endpush
@endsection
