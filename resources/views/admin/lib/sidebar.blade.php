<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">


            <li>
                <a href="{{ url('admin/dashboard') }}">
                    <i class="fa fa-dashboard fa-fw"></i> Dashboard
                </a>
            </li>


            <li>
                <a href="{{ url('/') }}" target="_blank">
                    <i class="fa fa-globe fa-fw"></i> Visit Site
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-list fa-fw"></i> Semester
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">

                    <li>
                        <a href="{{ route('admin.semester.index') }}">Semester List</a>
                    </li>

                    <li>
                        <a href="{{ route('admin.semester.create') }}">Add Semester</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="#">
                    <i class="fa fa-list fa-fw"></i> Assign
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">

                    {{--                    <li>--}}
                    {{--                        <a href="{{ route('admin.assign.index') }}">Assign List</a>--}}
                    {{--                    </li>--}}

                    <li>
                        <a href="{{ route('admin.assign.create') }}">Add New</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-list fa-fw"></i> Group
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">

                    <li>
                        <a href="{{ route('admin.group.index') }}">Group List</a>
                    </li>

{{--                    <li>--}}
                    {{--                        <a href="{{ route('admin.group.create') }}">Add Group</a>--}}
                    {{--                    </li>--}}
                </ul>
            </li>

            <li>
                <a href="#">
                    <i class="fa fa-users fa-fw"></i> Student
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">

                    <li>
                        <a href="{{ route('admin.student.index') }}">Student List</a>
                    </li>

                    <li>
                        <a href="{{ route('admin.student.create') }}">Add Student</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-users fa-fw"></i> Teacher
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">

                    <li>
                        <a href="{{ route('admin.teacher.index') }}">Teacher List</a>
                    </li>

                    <li>
                        <a href="{{ url('admin/supervisor/index') }}">Supervisor List</a>
                    </li>

                    <li>
                        <a href="{{ route('admin.teacher.create') }}">Add Teacher</a>
                    </li>
                </ul>
            </li>


            <li>
                <a href="#">
                    <i class="fa fa-list fa-fw"></i> Research Work
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">

                    <li>
                        <a href="{{ route('admin.research.index') }}">Research List</a>
                    </li>

                    <li>
                        <a href="{{ route('admin.research.create') }}">Add New</a>
                    </li>
                </ul>
            </li>

{{--            <li>--}}
{{--                <a href="#">--}}
{{--                    <i class="fa fa-list fa-fw"></i> Notice Category--}}
{{--                    <span class="fa arrow"></span>--}}
{{--                </a>--}}
{{--                <ul class="nav nav-second-level">--}}

{{--                    <li>--}}
{{--                        <a href="{{ route('admin.notice_category.index') }}">Notice Category</a>--}}
{{--                    </li>--}}

{{--                    <li>--}}
{{--                        <a href="{{ route('admin.notice_category.create') }}">Add New</a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}

{{--            <li>--}}
{{--                <a href="#">--}}
{{--                    <i class="fa fa-list fa-fw"></i> Notice--}}
{{--                    <span class="fa arrow"></span>--}}
{{--                </a>--}}
{{--                <ul class="nav nav-second-level">--}}

{{--                    <li>--}}
{{--                        <a href="{{ route('admin.notice_category.index') }}">Notice</a>--}}
{{--                    </li>--}}

{{--                    <li>--}}
{{--                        <a href="{{ route('admin.notice_category.create') }}">Add New</a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}

        </ul>
    </div>
</div>
