@extends('layout.admin.admin') @section('title','Assign') @section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Assign</h2>
            </div>
        </div>

        <!-- Content Header (Page header) -->
        <section class="content-header">
            @if(Session::has('success'))
                <p class="alert alert-success" id="message">{{ Session::get('success') }}</p>
            @endif @if(Session::has('error'))
                <p class="alert alert-warning" id="message">{{ Session::get('error') }}</p>
            @endif

            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-7">

                    </div>
                    <div class="col-sm-5">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}"
                                                           style="color: #000 !important;">Home</a></li>
                            <li class="breadcrumb-item">Assign list</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <section class="content">
                    <div class="row">
                        <div class="col-12">

                            <div class="card">
                                <div class="card-body">
                                    <form method="post" action="{{ url('admin/save_group') }}">
                                        <table id="example1" class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th width="20%">Area of Research</th>
                                                <th width="20%">Tentative Title</th>
                                                <th width="20%">Student's Details</th>
                                                <th width="10%">Applied on</th>
                                                <th width="30%">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($interests as $key=> $interest)
                                                <tr>


                                                    <td>{{ $interest->area_of_research }}</td>
                                                    <td>{{ $interest->tentative_title }}</td>
                                                    <td> {{ $interest->students_name     }} <br>
                                                        {{ $interest->students_id     }}</td>
                                                    <td>{{ date('d-m-Y',strtotime($interest->created_at)) }}</td>

                                                    <td>


                                                        @csrf
                                                        @method('post')
                                                        <select class="form-control" name="teacher_id[]"
                                                                style="width: 200px">
                                                            <option disabled required selected>Select</option>
                                                            @foreach($teachers as $teacher)

                                                                @if(in_array($teacher->id, \App\Http\Controllers\HelperController::getTeacherInitialswithOptions($interest->teacher_id)))
                                                                    <option
                                                                        value="{{ $teacher->id }}">{{ $teacher->name }}</option>
                                                                @endif
                                                            @endforeach

                                                        </select>
                                                        <input type="hidden" name="students_info[]"
                                                               value="{{ $interest->students_id }}">
                                                        <input type="hidden" name="interest_id[]"
                                                               value="{{ $interest->id }}">

                                                        <input type="hidden" name="semester_id[]"
                                                               value="{{ $interest->semester_id     }}">


                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>

                                            </tfoot>
                                        </table>
                                        <button class="btn btn-primary">Save</button>
                                    </form>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>

            </div>
        </section>
        <!-- /.content -->
    </div>
    @push('extra-css')
        <link rel="stylesheet"
              href="{{asset('asset/back/vendor/datatables/css/dataTables.bootstrap4.css')}}"> @endpush @push('extra-scripts')
        <script src="{{asset('asset/back/vendor/datatables/js/jquery.dataTables.js')}}"></script>
        <script src="{{asset('asset/back/vendor/datatables/js/dataTables.bootstrap4.js')}}"></script>
        <script>
            $(function () {
                $("#example1").DataTable();
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                });
            });
        </script>
    @endpush @endsection
