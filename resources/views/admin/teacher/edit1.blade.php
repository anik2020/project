@extends('layout.admin.admin') @section('title','Edit Teacher') @section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Edit Teacher</h2>
        </div>
    </div>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        @if(Session::has('success'))
        <p class="alert alert-success" id="message">{{ Session::get('success') }}</p>
        @endif @if(Session::has('error'))
        <p class="alert alert-warning" id="message">{{ Session::get('error') }}</p>
        @endif

        <div class="container-fluid">
            <div class="row mb-1">
                <div class="col-sm-7">

                </div>
                <div class="col-sm-5">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}" style="color: #000 !important;">Home</a></li>
                        <li class="breadcrumb-item">Edit Teacher</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content" style="min-height: 600px;">
        <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Edit Teacher</h3>
                </div>

                <form role="form" action="{{ route('admin.teacher.update',$teacher->id) }}" method="post" enctype="multipart/form-data">
                    @method('PUT') @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Enter name</label>
                            <input type="text" name="name" class="form-control" value="{{ $teacher->name }}" required>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Department</label>
                            <select name="department_id" class="form-control" id="">
                                <option value="">Select Department</option>
                                @foreach($departments as $department)
                                <option value="{{ $department->id }}" @if($teacher->department_id == $department->id)
                                    selected @endif>{{ $department->department_name }}</option>

                                @endforeach
                            </select>
                        </div>


                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="text" name="email" class="form-control" value="{{ $teacher->email }}" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Password</label>
                            <input type="password" name="password" class="form-control">
                        </div>


                        <div class="form-group">
                            <label for="exampleInputEmail1">Mobile</label>
                            <input type="text" name="mobile" class="form-control" value="{{ $teacher->mobile }}" required>
                        </div>


                        <div class="form-group">
                            <label for="exampleInputEmail1">Address</label>
                            <input type="text" name="address" class="form-control" value="{{ $teacher->address }}" required>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Select As Supervisor</label>
                            <select name="is_supervisor" id="" class="form-control">

                                <option value="0" @if($teacher->is_supervisor == 0)

                                    selected
                                    @endif>No</option>
                                <option value="1" @if($teacher->is_supervisor == 1)

                                    selected
                                    @endif>Yes</option>

                            </select>
                        </div>


                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
            <!-- /.card -->
            <!-- Form Element sizes --
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->


        <!-- /.card -->

</div>
</section>
</div>
@endsection