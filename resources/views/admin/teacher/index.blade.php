@extends('layout.admin.admin')
@section('title','Teacher')
@section('content')

    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Teacher</h2>
            </div>
        </div>
        <!-- START PAGE CONTENT-->
        <div class="page-content fade-in-up">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Teacher List</div>
                </div>
                <div class="ibox-body">
                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0"
                           width="100%">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Initial</th>
                            <th>Department</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Address</th>
                            <th>Action</th>

                        </tr>
                        </thead>

                        <tbody>
                        @foreach($teachers as $key=> $teacher)
                            <tr class="gradeX">

                                <td>{{ $teacher->name }}</td>
                                <td>{{ $teacher->initial }}</td>
                                <td>{{ $teacher->department->department_name }}</td>
                                <td>{{ $teacher->email }}</td>
                                <td>{{ $teacher->mobile }}</td>
                                <td>{{ $teacher->address }}</td>

                                <td>
                                    <div class="btn-group">
                                        <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Action
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ route('admin.teacher.edit',$teacher->id) }}">Edit
                                                    Teacher</a></li>
                                            <li>
                                                <a href="{{ route('admin.teacher.destroy',$teacher->id) }}"
                                                   onclick="event.preventDefault();
                                                       document.getElementById('vendor-delete-form{{ $teacher->id }}').submit();">
                                                    Delete Teacher
                                                </a>
                                                <form id="vendor-delete-form{{ $teacher->id }}"
                                                      action="{{ route('admin.teacher.destroy',$teacher->id) }}"
                                                      method="post" style="display: none;">
                                                    {{ csrf_field() }} @method('DELETE')
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!-- END PAGE CONTENT-->
        <footer class="page-footer">
            <div class="font-13">
                {{ date('Y') }} © <b>Admin</b> - All rights reserved.
            </div>

            <div class="to-top">
                <i class="fa fa-angle-double-up"></i>
            </div>
        </footer>
    </div>
    @push('extra-css')
        <link rel="stylesheet"
              href="{{asset('assets/back/vendors/DataTables/datatables.min.css')}}"> @endpush @push('extra-js')
        <script src="{{ asset('assets/back/vendors/DataTables/datatables.min.js') }}"></script>
        <script type="text/javascript">
            $(function () {
                $('.table').DataTable({
                    pageLength: 10,
                    //"ajax": './assets/demo/data/table_data.json',
                    /*"columns": [
                        { "data": "name" },
                        { "data": "office" },
                        { "data": "extn" },
                        { "data": "start_date" },
                        { "data": "salary" }
                    ]*/
                });
            })
        </script>
    @endpush @endsection
