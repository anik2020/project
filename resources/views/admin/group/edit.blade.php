@extends('layout.admin.admin') @section('title','Edit Group ') @section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Edit Group </h2>
            </div>
        </div>

        <!-- Content Header (Page header) -->
        <section class="content-header">
            @if(Session::has('success'))
                <p class="alert alert-success" id="message">{{ Session::get('success') }}</p>
            @endif @if(Session::has('error'))
                <p class="alert alert-warning" id="message">{{ Session::get('error') }}</p>
            @endif

            <div class="container-fluid">
                <div class="row mb-1">
                    <div class="col-sm-7">

                    </div>
                    <div class="col-sm-5">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}"
                                                           style="color: #000 !important;">Home</a></li>
                            <li class="breadcrumb-item">Edit Group</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="col-md-8">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Group </h3>
                    </div>

                    <form role="form" action="{{ route('admin.group.update',$group->id) }}" method="post"
                          enctype="multipart/form-data">
                        @method('PUT') @csrf
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Enter Group Name</label>
                                <input type="text" name="group_name"
                                       class="form-control @error('group_name') is-invalid @enderror"
                                       value="{{ $group->group_name }}" required>
                                @error('group_name')
                                <div class="alert alert-danger message">{{ $message }}</div>
                                @enderror

                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Department</label>
                                <select name="semester_id" class="form-control" id="">
                                    <option value="" disabled selected>Select Semester</option>
                                    @foreach($semesters as $semester)
                                        <option value="{{ $semester->id }}" @if($group->semester_id == $semester->id) selected="" @endif>{{ $semester->semester_name }}</option>
                                    @endforeach
                                </select>
                                @error('semester')
                                <div class="alert alert-danger message">{{ $message }}</div>
                                @enderror

                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Students Info</label>
                                <textarea name="students_info" rows="2"
                                          class="form-control @error('students_info') is-invalid @enderror"
                                          required>{{ $group->students_info }}</textarea>
                                @error('students_info')
                                <div class="alert alert-danger message">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Details</label>
                                <textarea name="details" rows="2"
                                          class="form-control @error('details') is-invalid @enderror"
                                          required>{{ $group->details }}</textarea>
                                @error('details')
                                <div class="alert alert-danger message">{{ $message }}</div>
                                @enderror
                            </div>


                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
                <!-- Form Element sizes --
            </div>
            <!-- /.card-body -->
            </div>
            <!-- /.card -->


            <!-- /.card -->

    </div>
    </section>
    </div>
@endsection
