<!DOCTYPE html>
<html lang="en">

<head>
    <title>@yield('title')</title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="" />
    <link rel="stylesheet" href="{{ asset('asset/front/css/bootstrap.min.css')}}">
    <link href="{{ asset('asset/front/css/style.css') }}" rel='stylesheet' type='text/css' />
    <!-- custom css -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
</head>

<body>
    <div class="container">
        <div style="background:#fff;box-shadow: 1px 1px 1px #ccc;">
            <div style="background:#673AB7; padding:12px">
                <div class="row">
                    <div class="col-lg-9">
                        <img style="margin: 0px; padding: 0px; width: 150px; height: 50px" src="{{ asset('asset/front/images/diulogo.jpg') }}" >

                        <h1 style="color:#fff; padding:0; margin:0; text-align:center;">Daffodil International University</h1>
                        <div style="color:#CDDC39; margin:0; font-size:28px; text-align: center">Asulia, Savar, Dhaka</div>
                    </div>
                    <div style="text-align:right;" class="col-lg-3">
                        <br>

                    </div>
                </div>
            </div>
            <!-- .navbar -->
           @include('layout.front.lib.nav')
            <!-- /.navbar -->
            <div style="text-align: justify;/* padding: 10px; */">
                @yield('content')
            </div>
            <div style="background:#fff;">
                <div class="row">
                    <div class="col-lg-12">
                        <marquee style="padding:8px;" class="bg-light">This site is under development</marquee>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-lg-5 col-md-9">
                        <div style="padding:5px;">
                            <p class="">© <span>{{ date('Y') }}</span> Copyright | Development Work
                               | <a href="#"> All rights Reserved</a>
                            </p>
                        </div>
                    </div>

                    <div class="col-lg-5 col-md-9">
                        Contact: 017XX-YYRREE | Email: something@gmail.com </div>
                </div>
            </div>
        </div>
    </div>

    <!-- copyright -->
    <!-- js -->
    <script src="{{ asset('asset/front/js/jquery.min.js') }}"></script>
    <script src="{{ asset('asset/front/js/bootstrap.min.js') }}"></script>
    <!-- //js -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
    <script>
        $(document).ready(function() {
            //FANCYBOX
            //https://github.com/fancyapps/fancyBox
            $(".fancybox").fancybox({
                openEffect: "none",
                closeEffect: "none"
            });
        });
    </script>

</body>

</html>
