<div class="row">
    <div class="col-lg-8">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item  active">
                    <img class="d-block w-100" src="{{ asset('asset/front/images/slider1.png') }}" max-height="500" alt="First slide">
                </div>

                <div class="carousel-item ">
                    <img class="d-block w-100" src="{{ asset('asset/front/images/slider2.png') }}" max-height="500" alt="First slide">
                </div>

            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <div style="padding:10px;padding-right:40px;" class="col-lg-4">
        <h2>Introduction<hr style="margin:0; padding:5px;"></h2>
        <p>
            Research Management System is an online cooperative platform for making research work better and hassle free.
            This was built for connecting supervisor, students as well as admin for simplifying research work. From this
            single platform students, teachers can perform their respective work from individual dashboard.
        </p>

    </div>
</div>
