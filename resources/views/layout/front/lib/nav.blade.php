<nav class="navbar navbar-full navbar-dark bg-light">
    <button class="navbar-toggler hidden-lg-up" type="button" data-toggle="collapse" data-target="#mainNavbarCollapse">
        &#9776; মেনু
    </button>
    <div class="collapse navbar-toggleable-md" id="mainNavbarCollapse">
        <ul class="nav navbar-nav pull-lg-left">
            <li class="nav-item active">
                <a class="nav-link" href="{{ url('/') }}"><i class="fas fa-home"></i> Home </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('research-work') }}">Current Research Work</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('previous-research-work') }}">Previous Research Work</a>
            </li>


            <li class="nav-item">
                <a class="nav-link" href="{{ url('teacher') }}">Teacher Login</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{ url('student') }}">Student Login</a>
            </li>


        </ul>
    </div>
</nav>
