<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="{{ url('student/dashboard') }}">
                    <i class="fa fa-dashboard fa-fw"></i> Dashboard
                </a>
            </li>

            <li>
                <a href="#">
                    <i class="fa fa-users fa-fw"></i> Teacher
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">

                    <li>
                        <a href="{{ route('student.teacher.index') }}">Teacher List</a>
                    </li>
                    <li>
                        <a href="{{ url('student/teacher/assigned') }}">Assigned Teacher List</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="#">
                    <i class="fa fa-user fa-fw"></i> Supervisor
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">

                    <li>
                        <a href="{{ url('student/interest/choice') }}">Give Choice</a>
                    </li>

                    <li>
                        <a href="{{ url('student/interest/choice_list') }}"> Choice List</a>
                    </li>
                    <li>
                        <a href="{{ url('student/teacher/assigned') }}">Assigned Teacher List</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="#">
                    <i class="fa fa-list fa-fw"></i> Meeting
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">

                    <li>
                        <a href="{{ route('student.meeting.index') }}">Meeting List</a>
                    </li>

                </ul>
            </li>


        </ul>
    </div>
</div>