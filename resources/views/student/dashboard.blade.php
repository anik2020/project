@extends('layout.student.student') @section('title','Dashboard') @section('content')
<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">Dashboard</h2>
    </div>
</div>
<div class="row">
    @if(Session::has('success'))
    <p class="alert alert-success message">{{ Session::get('success') }}</p>
    @endif @if(Session::has('error'))
    <p class="alert alert-warning message">{{ Session::get('error') }}</p>
    @endif
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-users fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">0</div>
                        <div>Total Registered User</div>
                    </div>
                </div>
            </div>
            <a href="{{  route('admin.teacher.index') }}">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>


</div>

<br>

</div>

@endsection
