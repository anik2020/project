@extends('layout.front.home')
@section('title','Research work')
@section('content')

    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 96%;
            margin: 0 auto;
            color: #000;
            border: 1px solid #000;
            margin-bottom: 10px;
        }

        th {
            text-align: center;
        }

        td, th {
            padding: 5px;
            border: 1px solid #000;
        }

        tr {
            border: 1px solid #000;

        }

        h2{
            padding: 5px;
            margin-top: 10px;
            text-align: center;
            border-bottom: 1px solid #2b2d2f;
            border-top: 1px solid #2b2d2f;
        }

    </style>
    </head>
    <body>

    <h2>Research Works</h2>

    <table>
        </thead>
        <tr>
            <th style="text-align: center;">Serial</th>
            <th>Research Topic</th>
            <th>Description</th>
            <th>Supervisor</th>
            <th style="text-align: center;">Created</th>
        </tr>
        <thead>

        <tbody>
        @foreach($researches as $key=> $research)
            <tr>

                <td style="text-align:center;">{{ ++$key }}</td>
                <td>{{ $research->topic }}</td>
                <td>{{ $research->description }}</td>
                <td>{{ $research->teacher->name }}</td>
                <td style="text-align:center;">{{ date('d-m-Y',strtotime($research->created_at)) }}</td>

            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
