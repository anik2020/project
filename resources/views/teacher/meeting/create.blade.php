@extends('layout.teacher.teacher') @section('title','Add Meeting') @section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Add Meeting</h2>
        </div>
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        @if(Session::has('success'))
        <p class="alert alert-success" id="message">{{ Session::get('success') }}</p>
        @endif @if(Session::has('error'))
        <p class="alert alert-warning" id="message">{{ Session::get('error') }}</p>
        @endif

        <div class="container-fluid">
            <div class="row mb-1">
                <div class="col-sm-7">

                </div>
                <div class="col-sm-5">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('$assigndashboard') }}" style="color: #000 !important;">Home</a></li>
                        <li class="breadcrumb-item">Add Meeting</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Add Meeting</h3>
                </div>
                <form role="form" action="{{ route('teacher.meeting.store') }}" method="post" enctype="multipart/form-data">
                    @method('POST') @csrf
                    <div class="card-body">

                         <div class="form-group">
                            <label for="exampleInputEmail1">Day</label>
                             <select name="date" class="form-control" id="">
                                 <option value="" disabled selected>Select Day</option>
                                 @foreach($days as $day)
                                    <option value="{{ $day }}">{{ $day }}</option>
                                 @endforeach
                             </select>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Enter Time</label>
                            <select name="hour"  class="form-control" style="display: inline-block; width: 10%;">
                                <option disabled selected>Hour</option>
                                <?php for($i=1; $i<=12; $i++){ ?>
                                    <?php if($i < 10): ?>
                                          <option value="0<?= $i ?>">0<?= $i ?></option>
                                    <?php else: ?>
                                        <option value="<?= $i ?>"><?= $i ?></option>
                                    <?php endif ?>

                                <?php    }  ?>
                            </select>
                             <select name="minute"  class="form-control" style="display: inline-block; width: 10%;">
                                <option disabled selected>Minute</option>
                                 <?php for($i=1; $i<=60; $i++){ ?>
                                 <?php if($i==60): ?>
                                     <?php if($i < 10): ?>
                                     <option value="0<?= $i ?>">0<?= $i ?></option>
                                     <?php else: ?>
                                     <option value="<?= $i ?>"><?= $i ?></option>
                                     <?php endif ?>
                                 <?php else:  ?>
                                         <?php if($i < 10): ?>
                                         <option value="0<?= $i ?>">0<?= $i ?></option>
                                     <?php else: ?>
                                         <option value="<?= $i ?>"><?= $i ?></option>
                                     <?php endif ?>
                                 <?php endif ?>
                                 <?php    }
                                 ?>
                            </select>

                            <select name="amorpm"  class="form-control" style="display: inline-block; width: 10%;">
                                <option value="AM">AM</option>
                                <option value="PM">PM</option>

                            </select>

                        </div>

                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
            <!-- /.card -->
            <!-- Form Element sizes --
        </div>
        <!-- /.card-body -->
        </div>
        <!-- /.card -->


        <!-- /.card -->

</div>
</section>
</div>
@endsection
