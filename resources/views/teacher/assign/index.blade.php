@extends('layout.teacher.teacher') @section('title','Group') @section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Group List</h2>
            </div>
        </div>

        <!-- Content Header (Page header) -->
        <section class="content-header">
            @if(Session::has('success'))
                <p class="alert alert-success" id="message">{{ Session::get('success') }}</p>
            @endif @if(Session::has('error'))
                <p class="alert alert-warning" id="message">{{ Session::get('error') }}</p>
            @endif

            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-7">

                    </div>
                    <div class="col-sm-5">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}"
                                                           style="color: #000 !important;">Home</a></li>
                            <li class="breadcrumb-item">Group list</li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <section class="content">
                    <div class="row">
                        <div class="col-12">

                            <div class="card">
                                <div class="card-body">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Tentative Title</th>
                                            <th>Semester Name</th>
                                            <th>Student's Info</th>
                                            <th>Added On</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($groups as $key=> $group)
                                            <tr>

                                                <td>{{ ++$key }}</td>
                                                <td>{{ $group->interest->tentative_title }}</td>
                                                <td>{{ $group->semester->semester_name }}</td>
                                                <td>{{ $group->students_info }}</td>

                                                <td>{{ date('d-m-Y',strtotime($group->created_at)) }}</td>

                                            </tr>
                                        @endforeach
                                        </tbody>

                                        </tfoot>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>

            </div>
        </section>
        <!-- /.content -->
    </div>
    @push('extra-css')
        <link rel="stylesheet"
              href="{{asset('asset/back/vendor/datatables/css/dataTables.bootstrap4.css')}}"> @endpush @push('extra-scripts')
        <script src="{{asset('asset/back/vendor/datatables/js/jquery.dataTables.js')}}"></script>
        <script src="{{asset('asset/back/vendor/datatables/js/dataTables.bootstrap4.js')}}"></script>
        <script>
            $(function () {
                $("#example1").DataTable();
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                });
            });
        </script>
    @endpush @endsection
