<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">


            <li>
                <a href="{{ url('teacher/dashboard') }}">
                    <i class="fa fa-dashboard fa-fw"></i> Dashboard
                </a>
            </li>


            <li>
                <a href="{{ url('/') }}" target="_blank">
                    <i class="fa fa-globe fa-fw"></i> Visit Site
                </a>
            </li>

{{--            <li>--}}
{{--                <a href="#">--}}
{{--                    <i class="fa fa-list fa-fw"></i> Meeting--}}
{{--                    <span class="fa arrow"></span>--}}
{{--                </a>--}}
{{--                <ul class="nav nav-second-level">--}}

{{--                    <li>--}}
{{--                        <a href="{{ route('teacher.assigned_group') }}">Group List</a>--}}
{{--                    </li>--}}

{{--                </ul>--}}
{{--            </li>--}}
            <li>
                <a href="#">
                    <i class="fa fa-list fa-fw"></i> Assigned Group
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">

                    <li>
                        <a href="{{ route('teacher.assigned_group') }}">Group List</a>
                    </li>

                </ul>
            </li>


        </ul>
    </div>
</div>
