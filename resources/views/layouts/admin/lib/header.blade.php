<header class="header">
    <div class="page-brand">
        <a class="link" href="{{ route('admin.dashboard') }}">
            <span class="brand">Admin
                <span class="brand-tip">PANEL</span>
            </span>
            <span class="brand-mini">AP</span>
        </a>
    </div>
    <div class="flexbox flex-1">
        <!-- START TOP-LEFT TOOLBAR-->
        <ul class="nav navbar-toolbar">
            <li>
                <a class="nav-link sidebar-toggler js-sidebar-toggler"><i class="ti-menu"></i></a>
            </li>

        </ul>
        <!-- END TOP-LEFT TOOLBAR-->
        <!-- START TOP-RIGHT TOOLBAR-->
        <ul class="nav navbar-toolbar">


            <li class="dropdown dropdown-user">
                <a class="nav-link dropdown-toggle link" data-toggle="dropdown">
                    <img src="{{ asset('assets/back/img/admin-avatar.png')}}" />
                    <span></span>Admin<i class="fa fa-angle-down m-l-5"></i></a>
                <ul class="dropdown-menu dropdown-menu-right">

                    <li class="dropdown-divider"></li>
                    <!-- <a class="dropdown-item" href="login.html"><i class="fa fa-power-off"></i>Logout</a> -->

                    <a class="dropdown-item" title href="{{ url('/admin/logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                        <i class="fa fa-power-off"></i>Logout
                    </a>

                    <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>

                </ul>
            </li>
        </ul>
        <!-- END TOP-RIGHT TOOLBAR-->
    </div>
</header>