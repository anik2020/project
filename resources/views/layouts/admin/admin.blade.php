<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width initial-scale=1.0" />
    <title>@yield('title')</title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="{{ asset('assets/back/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{ asset('assets/back/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" />
    <link href="{{ asset('assets/back/vendors/themify-icons/css/themify-icons.css')}}" rel="stylesheet" />
    <!-- PLUGINS STYLES-->
    <link href="{{ asset('assets/back/vendors/jvectormap/jquery-jvectormap-2.0.3.css')}}" rel="stylesheet" />
    <!-- THEME STYLES-->
    @stack('extra-css')
    <link href="{{ asset('assets/back/css/main.min.css')}}" rel="stylesheet" />
    <!-- PAGE LEVEL STYLES-->
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->

        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('layouts.admin.lib.header') @include('layouts.admin.lib.sidebar')
        <!-- END SIDEBAR-->
        @yield('content')
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->

    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <!-- <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div> -->
    <!-- END PAGA BACKDROPS-->
    <!-- CORE PLUGINS-->
    <script src="{{ asset('assets/back/vendors/jquery/dist/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{
                asset('assets/back/vendors/popper.js/dist/umd/popper.min.js')
            }}" type="text/javascript"></script>
    <script src="{{
                asset('assets/back/vendors/bootstrap/dist/js/bootstrap.min.js')
            }}" type="text/javascript"></script>
    <script src="{{
                asset('assets/back/vendors/metisMenu/dist/metisMenu.min.js')
            }}" type="text/javascript"></script>
    <script src="{{
                asset(
                    'assets/back/vendors/jquery-slimscroll/jquery.slimscroll.min.js'
                )
            }}" type="text/javascript"></script>
    <!-- PAGE LEVEL PLUGINS-->
    <script src="{{ asset('assets/back/vendors/chart.js/dist/Chart.min.js') }}" type="text/javascript"></script>
    <script src="{{
                asset(
                    'assets/back/vendors/jvectormap/jquery-jvectormap-2.0.3.min.js'
                )
            }}" type="text/javascript"></script>
    <script src="{{
                asset(
                    'assets/back/vendors/jvectormap/jquery-jvectormap-world-mill-en.js'
                )
            }}" type="text/javascript"></script>
    <script src="{{
                asset(
                    'assets/back/vendors/jvectormap/jquery-jvectormap-us-aea-en.js'
                )
            }}" type="text/javascript"></script>
    <!-- CORE SCRIPTS-->
    @stack('extra-js')
    <script src="{{ asset('assets/back/js/app.min.js') }}" type="text/javascript"></script>
    <!-- PAGE LEVEL SCRIPTS-->
    <script src="{{ asset('assets/back/js/scripts/dashboard_1_demo.js') }}" type="text/javascript"></script>
</body>

</html>