<?php

Route::namespace('Teacher')->group(function () {
    Route::get('home', function(){
        return redirect('teacher/dashboard');
    });
    Route::get('dashboard', 'TeacherController@dashboard')->name('dashboard');
    Route::resource('meeting','MeetingController')->except('show');
    Route::get('assign/group', 'AssignController@assigned_group')->name('assigned_group');
    Route::get('assign/group/view_assigned_students/{id}', 'AssignController@view_assigned');
//    Route::get('assign/group/edit/{id}', 'AssignController@edit_group');
//    Route::get('assign/group/delete/{id}', 'AssignController@delete_group');

});

