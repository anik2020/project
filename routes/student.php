<?php
Route::namespace('Student')->group(function () {
    Route::get('dashboard', 'StudentController@dashboard')->name('dashboard');
    //Route::get('teacher/delete/{id}', 'TeacherController@delete');
    Route::resource('teacher', 'TeacherController')->except(['show']);
    Route::get('teacher/assigned', 'TeacherController@assigned');
    Route::resource('student', 'StudentController')->except(['show']);
    Route::resource('meeting', 'MeetingController')->except(['show']);
    Route::get('setting', 'StudentController@setting')->name('setting');
    Route::match(['get', 'post'], 'interest/choice', 'InterestController@choice');
    Route::get('interest/choice_list', 'InterestController@choice_list');
});