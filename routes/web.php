<?php


Route::get('admin', function () {
    return redirect('admin/login');
});

Route::get('teacher', function () {
    return redirect('teacher/login');
});

Route::get('student', function () {
    return redirect('student/login');
});
Route::get('/admin', function () {
    return redirect('admin/login');
});

Route::namespace('Front')->group(function(){
    Route::get('/','HomeController@index');
    Route::get('/research-work','ResearchController@index');
    Route::get('/previous-research-work','ResearchController@previous');

});

Route::group(['prefix' => 'admin'], function () {

    Route::get('/login', 'Admin\Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Admin\Auth\LoginController@login');
    Route::post('/logout', 'Admin\Auth\LoginController@logout')->name('logout');

    Route::get('/register', 'Admin\Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'Admin\Auth\RegisterController@register');

    Route::post('/password/email', 'Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'Admin\Auth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'Admin\Auth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'student'], function () {
    Route::get('/login', 'Student\Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Student\Auth\LoginController@login');
    Route::post('/logout', 'Student\Auth\LoginController@logout')->name('logout');

    Route::get('/register', 'Student\Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'Student\Auth\RegisterController@register');

    Route::post('/password/email', 'Student\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'Student\Auth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'Student\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'Student\Auth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'teacher'], function () {
    Route::get('/login', 'Teacher\Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Teacher\Auth\LoginController@login');
    Route::post('/logout', 'Teacher\Auth\LoginController@logout')->name('logout');

    Route::get('/register', 'Teacher\Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'Teacher\Auth\RegisterController@register');

    Route::post('/password/email', 'Teacher\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'Teacher\Auth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'Teacher\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'Teacher\Auth\ResetPasswordController@showResetForm');
});
