<?php

Route::namespace('Admin')->group(function () {
    Route::get('dashboard', 'AdminController@dashboard')->name('dashboard');
    Route::resource('teacher', 'TeacherController')->except(['show']);
    Route::get('supervisor/index', 'TeacherController@supervisor_index');
    Route::resource('student', 'StudentController')->except(['show']);
    Route::resource('assign', 'AssignController')->except(['show']);
    Route::resource('research', 'ResearchController')->except(['show']);
    Route::resource('group', 'GroupController')->except(['show']);
    Route::resource('semester', 'SemesterController')->except(['show']);
    Route::resource('notice_category', 'NoticeCategoryController')->except(['show']);
    Route::get('notice_category/delete/{id}', 'NoticeCategoryController@delete');
    Route::resource('notice', 'NoticeController')->except(['show']);
    Route::get('setting', 'AdminController@setting')->name('setting');
    Route::post('save_group', 'GroupController@save_interest_group');
    Route::get('choice_list','InterestController@choice_list')->name('choice_list');
    Route::get('choice','InterestController@choice');
});
