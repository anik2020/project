-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: shapla
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.10-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,'Ariful Islam','admin@gmail.com','$2y$10$50sKq9q2cOS0.phgaFkv..F.Eb1plpGzTJu//NKJnNzOMoY9B9fMe',NULL,NULL,NULL,'pjq46NEJmGb32fkYKLfwIyNCBQC99vGUsgdMOCVq0PaXK9jQDhOjzgo0Zwt5','2020-03-27 12:38:30','2020-03-27 12:38:30');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assigns`
--

DROP TABLE IF EXISTS `assigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assigns` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `teacher_id` bigint(20) unsigned NOT NULL,
  `group_id` bigint(20) unsigned NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assigns_teacher_id_foreign` (`teacher_id`),
  KEY `assigns_group_id_foreign` (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assigns`
--

LOCK TABLES `assigns` WRITE;
/*!40000 ALTER TABLE `assigns` DISABLE KEYS */;
/*!40000 ALTER TABLE `assigns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `department_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departments`
--

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` VALUES (1,'Eligendi voluptatem.','2020-03-27 12:38:30','2020-03-27 12:38:30'),(2,'Adipisci modi velit.','2020-03-27 12:38:30','2020-03-27 12:38:30'),(3,'Omnis quaerat velit.','2020-03-27 12:38:30','2020-03-27 12:38:30'),(4,'Totam blanditiis.','2020-03-27 12:38:30','2020-03-27 12:38:30'),(5,'Fuga perferendis.','2020-03-27 12:38:30','2020-03-27 12:38:30');
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `group_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `teacher_id` int(11) DEFAULT NULL,
  `interest_id` int(11) NOT NULL,
  `semester_id` bigint(20) unsigned NOT NULL,
  `students_info` varchar(199) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (28,'',6,8,5,'161-15-935, 161-88-234, 161-43-98','','','2020-03-28 12:47:43','2020-03-28 12:47:43'),(27,'',6,11,2,'987,979,08','','','2020-03-28 12:46:06','2020-03-28 12:46:06'),(25,'',6,11,2,'987,979,08','','','2020-03-28 12:44:39','2020-03-28 12:44:39'),(26,'',5,9,5,'Karim, Rahim, Aziz','','','2020-03-28 12:46:06','2020-03-28 12:46:06'),(24,'',5,9,5,'Karim, Rahim, Aziz','','','2020-03-28 12:44:39','2020-03-28 12:44:39'),(22,'',5,7,7,'161-14-987, 161-15-928, 161-15-934','','','2020-03-28 12:44:39','2020-03-28 12:44:39'),(23,'',5,10,2,'161-15-946, 161-15-947, 16-15-948','','','2020-03-28 12:44:39','2020-03-28 12:44:39'),(21,'',6,8,5,'161-15-935, 161-88-234, 161-43-98','','','2020-03-28 12:44:39','2020-03-28 12:44:39'),(29,'',6,7,7,'161-14-987, 161-15-928, 161-15-934','','','2020-03-28 12:47:43','2020-03-28 12:47:43'),(30,'',5,10,2,'161-15-946, 161-15-947, 16-15-948','','','2020-03-28 12:47:43','2020-03-28 12:47:43'),(31,'',5,7,7,'161-14-987, 161-15-928, 161-15-934','','','2020-03-28 12:48:38','2020-03-28 12:48:38'),(32,'',2,10,2,'161-15-946, 161-15-947, 16-15-948','','','2020-03-28 12:48:38','2020-03-28 12:48:38'),(33,'',5,9,5,'Karim, Rahim, Aziz','','1','2020-03-28 12:48:38','2020-03-28 12:48:38');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interests`
--

DROP TABLE IF EXISTS `interests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interests` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `semester_id` bigint(20) unsigned DEFAULT NULL,
  `teacher_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `students_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `student_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `students_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area_of_research` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tentative_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `interests_semester_id_foreign` (`semester_id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interests`
--

LOCK TABLES `interests` WRITE;
/*!40000 ALTER TABLE `interests` DISABLE KEYS */;
INSERT INTO `interests` VALUES (11,2,'1,6,4','ABC, DEF, RHI','12','987,979,08','Database','Tentitive Title',1,'2020-03-28 08:24:32','2020-03-28 12:46:06'),(10,2,'5,2,4','Ariful islam, Khayrul islam, Asadujjaman','12','161-15-946, 161-15-947, 16-15-948','Data Mining','Tentative Title',1,'2020-03-28 08:09:49','2020-03-28 12:48:38'),(9,5,'5,','Baker Bhuiyan','10','Karim, Rahim, Aziz','Database','Character Reading using Data Mining Algorithm',1,'2020-03-27 22:01:34','2020-03-28 12:48:38'),(8,5,'6,2,4','Azizul Hakim, Ibrahim Bhuiyan, Asadujjaman','10','161-15-935, 161-88-234, 161-43-98','Algorithm','Case Study Using Machine Learning',1,'2020-03-27 22:00:46','2020-03-28 12:47:43'),(7,7,'1,5,6','Ariful Islam, Kamrul Islam, Raisul Islam','12','161-14-987, 161-15-928, 161-15-934','Data Mining','Data Analysis Management',1,'2020-03-27 21:55:53','2020-03-28 12:48:38');
/*!40000 ALTER TABLE `interests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetings`
--

DROP TABLE IF EXISTS `meetings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `meetingtime_id` bigint(20) unsigned NOT NULL,
  `student_id` bigint(20) unsigned NOT NULL,
  `teacher_id` bigint(20) unsigned NOT NULL,
  `date` date NOT NULL,
  `time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `meetings_meetingtime_id_foreign` (`meetingtime_id`),
  KEY `meetings_student_id_foreign` (`student_id`),
  KEY `meetings_teacher_id_foreign` (`teacher_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetings`
--

LOCK TABLES `meetings` WRITE;
/*!40000 ALTER TABLE `meetings` DISABLE KEYS */;
INSERT INTO `meetings` VALUES (1,0,0,5,'0000-00-00','23:38:32PM','0','2020-03-27 12:38:32','2020-03-27 12:38:32'),(2,0,0,3,'0000-00-00','22:38:32PM','0','2020-03-27 12:38:32','2020-03-27 12:38:32'),(3,0,0,3,'0000-00-00','19:38:32PM','0','2020-03-27 12:38:32','2020-03-27 12:38:32'),(4,0,0,2,'0000-00-00','19:38:32PM','0','2020-03-27 12:38:32','2020-03-27 12:38:32'),(5,0,0,3,'0000-00-00','21:38:32PM','0','2020-03-27 12:38:32','2020-03-27 12:38:32'),(6,0,0,4,'0000-00-00','22:38:32PM','0','2020-03-27 12:38:32','2020-03-27 12:38:32'),(7,0,0,2,'0000-00-00','20:38:32PM','0','2020-03-27 12:38:32','2020-03-27 12:38:32'),(8,0,0,1,'0000-00-00','22:38:32PM','0','2020-03-27 12:38:32','2020-03-27 12:38:32'),(9,0,0,1,'0000-00-00','20:38:32PM','0','2020-03-27 12:38:32','2020-03-27 12:38:32'),(10,0,0,2,'0000-00-00','20:38:32PM','0','2020-03-27 12:38:32','2020-03-27 12:38:32');
/*!40000 ALTER TABLE `meetings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetingtimes`
--

DROP TABLE IF EXISTS `meetingtimes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetingtimes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `day` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `teacher_id` bigint(20) unsigned NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `meetingtimes_teacher_id_foreign` (`teacher_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetingtimes`
--

LOCK TABLES `meetingtimes` WRITE;
/*!40000 ALTER TABLE `meetingtimes` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetingtimes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2019_08_19_001000_create_admins_table',1),(5,'2019_11_02_001547_create_settings_table',1),(6,'2019_12_25_161621_create_admin_password_resets_table',1),(7,'2020_01_08_102842_create_pages_table',1),(8,'2020_01_24_071716_create_semesters_table',1),(9,'2020_02_01_085356_create_groups_table',1),(10,'2020_02_01_184321_create_departments_table',1),(11,'2020_02_02_084523_create_students_table',1),(12,'2020_02_02_084525_create_student_password_resets_table',1),(13,'2020_02_02_084526_create_teachers_table',1),(14,'2020_02_02_084527_create_teacher_password_resets_table',1),(15,'2020_02_09_190219_create_meetingtimes_table',1),(16,'2020_02_09_190221_create_meetings_table',1),(17,'2020_02_27_132059_create_student_meetings_table',1),(18,'2020_02_28_085117_create_assigns_table',1),(19,'2020_02_29_050505_create_researchworks_table',1),(20,'2020_03_06_210519_create_notice_categories_table',1),(21,'2020_03_06_210529_create_notices_table',1),(22,'2020_03_13_163930_create_interests_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notice_categories`
--

DROP TABLE IF EXISTS `notice_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notice_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notice_categories`
--

LOCK TABLES `notice_categories` WRITE;
/*!40000 ALTER TABLE `notice_categories` DISABLE KEYS */;
INSERT INTO `notice_categories` VALUES (1,'Mollitia aut enim.','2020-03-27 12:38:32','2020-03-27 12:38:32'),(2,'Iure reprehenderit.','2020-03-27 12:38:32','2020-03-27 12:38:32'),(3,'Incidunt omnis.','2020-03-27 12:38:32','2020-03-27 12:38:32'),(4,'Error autem sint.','2020-03-27 12:38:32','2020-03-27 12:38:32'),(5,'Aliquid quia.','2020-03-27 12:38:32','2020-03-27 12:38:32');
/*!40000 ALTER TABLE `notice_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notices`
--

DROP TABLE IF EXISTS `notices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notices` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notice_category_id` bigint(20) unsigned NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attach` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notices_notice_category_id_foreign` (`notice_category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notices`
--

LOCK TABLES `notices` WRITE;
/*!40000 ALTER TABLE `notices` DISABLE KEYS */;
INSERT INTO `notices` VALUES (1,'Aut rerum odit sed eum doloribus ut aspernatur repellat.',3,'Eos qui quam a deserunt velit sunt qui. Quo sapiente aut cumque quia.','Possimus vel fugit aliquam autem aut minima. Eos quia est distinctio illo..docx','2020-03-27 12:38:32','2020-03-27 12:38:32'),(2,'Dolor cum optio ipsum excepturi vel quia.',4,'Dolorem temporibus nihil accusamus recusandae. Eius fugit quasi eaque natus ea numquam.','Quia veniam asperiores est sunt. Ipsa iure at veniam et deserunt..jpg','2020-03-27 12:38:32','2020-03-27 12:38:32'),(3,'Culpa quo est quae dolores nam.',1,'Autem aliquam voluptates soluta et. Rerum velit incidunt et. Iste sunt voluptas sed non.','Nihil dolor iste assumenda reiciendis. Vel voluptatem est ut esse..pdf','2020-03-27 12:38:32','2020-03-27 12:38:32'),(4,'Cumque dolore velit nisi sapiente.',3,'Nam reiciendis cum recusandae. Quo voluptate vitae consectetur nisi quo dolor eaque.','Quis necessitatibus quos saepe tempore unde. Excepturi nesciunt quas et consequatur est..jpg','2020-03-27 12:38:32','2020-03-27 12:38:32'),(5,'Reiciendis ut accusantium corrupti aperiam corrupti.',3,'Distinctio repudiandae similique repellendus. Eius animi voluptate cupiditate.','Modi enim odio ab cupiditate sed molestiae. Hic velit qui eum et ea vel..pdf','2020-03-27 12:38:32','2020-03-27 12:38:32'),(6,'Dolore nostrum qui sed aliquid dolor.',1,'Veniam saepe nihil enim. Non excepturi iusto autem quis. Eius quis ducimus officia et.','Aut modi reiciendis blanditiis qui et quis sed. Aut aut quasi velit ut..pdf','2020-03-27 12:38:32','2020-03-27 12:38:32'),(7,'Dolore quia quia illo non non.',4,'Reiciendis et eius accusantium qui. Ab dolorem facilis cupiditate. At mollitia corrupti at.','Ducimus blanditiis autem ipsum pariatur. Eveniet et repudiandae perferendis ipsam magnam sunt..jpg','2020-03-27 12:38:32','2020-03-27 12:38:32'),(8,'Explicabo assumenda voluptates voluptas harum sed.',3,'Dolores amet adipisci quo qui deserunt. Architecto fugiat blanditiis et totam eos odit.','Nihil recusandae sunt ut qui rerum. Velit unde modi voluptate velit minus vel..docx','2020-03-27 12:38:32','2020-03-27 12:38:32'),(9,'Nihil facilis vel dolores sint error architecto quaerat.',2,'Doloribus aut commodi sed. Quisquam et et enim est magni et.','Nihil nesciunt sed ratione. Vitae consequatur consequatur tempora architecto..jpg','2020-03-27 12:38:32','2020-03-27 12:38:32'),(10,'Ipsum consequatur minus odio et.',4,'Aliquid accusamus cupiditate praesentium non. Quibusdam et ex libero autem.','Exercitationem omnis praesentium ipsam et eos. Expedita qui excepturi nulla mollitia..pdf','2020-03-27 12:38:32','2020-03-27 12:38:32');
/*!40000 ALTER TABLE `notices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `researchworks`
--

DROP TABLE IF EXISTS `researchworks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `researchworks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `topic` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_id` bigint(20) unsigned DEFAULT NULL,
  `teacher_id` bigint(20) unsigned DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `researchworks_group_id_foreign` (`group_id`),
  KEY `researchworks_teacher_id_foreign` (`teacher_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `researchworks`
--

LOCK TABLES `researchworks` WRITE;
/*!40000 ALTER TABLE `researchworks` DISABLE KEYS */;
INSERT INTO `researchworks` VALUES (1,'Nostrum adipisci et totam fugit id consequatur explicabo officia. Harum illo atque sed nihil.','Eius vel assumenda repellendus sed et molestiae possimus. Id et eveniet asperiores in.',3,2,2,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(2,'Accusantium et esse omnis. Quo eum at quam. Aut amet sunt autem et at accusantium magnam.','Nobis dolor autem ut velit quia et. Id fuga ea incidunt. Eos alias distinctio quo veritatis magnam.',1,4,2,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(3,'Numquam deserunt qui molestiae natus. Quidem minima sint qui vitae.','Sapiente libero repellat suscipit totam blanditiis. Molestias fugit vitae aliquam. Eum a et a.',3,3,3,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(4,'Sapiente sed rerum illo voluptatem aut nisi incidunt. Porro eos ut quia consequatur magnam.','Quis corrupti omnis qui temporibus. Ut facere omnis vel ad. Velit laboriosam sed autem inventore.',4,4,3,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(5,'Minus qui aliquid nulla voluptatibus ullam quo id ullam. Atque nostrum iste qui minus iste.','Quod esse ducimus ducimus. Numquam eum id autem molestiae.',3,5,1,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(6,'Molestias voluptatem earum eligendi ab. Ipsam aut officia a debitis et.','Maiores nobis occaecati deserunt. Quidem sint earum iusto nulla.',1,5,3,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(7,'Enim unde vel voluptas architecto voluptas. Et sed ut dolor qui est voluptatum veritatis.','Aut sed aut atque et. Quasi praesentium qui sapiente ducimus. Fugit eum voluptas sit sint.',1,4,2,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(8,'Eius sunt nesciunt voluptate deserunt. Qui est a aut qui id recusandae nesciunt.','Et voluptatem nulla aut mollitia laudantium et quod. Perspiciatis exercitationem amet sed.',2,1,1,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(9,'Eius qui ut qui. Consequatur totam corporis dicta officia. Reiciendis et magnam eos dolorum.','Culpa rerum quo et. Repudiandae quo assumenda repellat nihil iure. Ut qui eaque ipsam et.',3,3,3,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(10,'Laborum non quia quis. Est in perferendis deserunt quis pariatur sit quam.','Laudantium sed enim eligendi. Esse ipsam porro non rerum adipisci.',4,5,1,'2020-03-27 12:38:32','2020-03-27 12:38:32');
/*!40000 ALTER TABLE `researchworks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `semesters`
--

DROP TABLE IF EXISTS `semesters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `semesters` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `semester_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `semesters`
--

LOCK TABLES `semesters` WRITE;
/*!40000 ALTER TABLE `semesters` DISABLE KEYS */;
INSERT INTO `semesters` VALUES (2,'Spring 2019',NULL,NULL),(3,'Fall 2019',NULL,NULL),(4,'Summer 2019',NULL,'2020-03-28 07:50:53'),(5,'Spring 2020',NULL,NULL),(6,'Summer 2020',NULL,NULL),(7,'Fall 2020',NULL,NULL);
/*!40000 ALTER TABLE `semesters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pinterest` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_meetings`
--

DROP TABLE IF EXISTS `student_meetings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_meetings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` bigint(20) unsigned NOT NULL,
  `meeting_id` bigint(20) unsigned NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `student_meetings_student_id_foreign` (`student_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_meetings`
--

LOCK TABLES `student_meetings` WRITE;
/*!40000 ALTER TABLE `student_meetings` DISABLE KEYS */;
INSERT INTO `student_meetings` VALUES (1,1,3,1,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(2,10,3,0,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(3,5,8,1,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(4,7,4,1,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(5,9,10,1,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(6,7,5,1,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(7,7,7,1,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(8,5,3,0,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(9,2,5,1,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(10,9,8,1,'2020-03-27 12:38:32','2020-03-27 12:38:32');
/*!40000 ALTER TABLE `student_meetings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_password_resets`
--

DROP TABLE IF EXISTS `student_password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `student_password_resets_email_index` (`email`),
  KEY `student_password_resets_token_index` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_password_resets`
--

LOCK TABLES `student_password_resets` WRITE;
/*!40000 ALTER TABLE `student_password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `student_password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_id` bigint(20) unsigned DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `students_email_unique` (`email`),
  KEY `students_department_id_foreign` (`department_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (1,'Ernesto Batz',2,'max.oberbrunner@example.net',NULL,'519-296-3165 x691','$2y$10$QAsjZhMlmeSHqLAnDIGYEeb7azIr5JMu1BYqodJ6j/vW87tlIm/Ve','543 Mraz Extensions\nLangburgh, ME 16987',NULL,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(2,'Quinten Ernser',2,'heidi.ferry@example.com',NULL,'1-417-305-9925','$2y$10$KyR2Bj02vv6ukm6Jgbth5.tO4UU6bNMQgkkjgnfmg9puFJa24WlyO','3686 Mohr Estate Suite 174\nOswaldoport, OR 60063',NULL,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(3,'Columbus Koelpin',4,'jordyn.zieme@example.org',NULL,'796-330-7762 x5770','$2y$10$ZfcjHPtX7djngP11Fnyp7.Oc/IlNeCwvU3UbKgGfpdAXUMjGARK9a','52126 Maye Mission\nHaagside, GA 95052',NULL,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(4,'Ernie Macejkovic',5,'jayce77@example.net',NULL,'(938) 681-6424','$2y$10$bOlU8dTSSQF4rB91olwMt.yX91fw7GH.mZrD1ssKHCm8ljqc3qnBe','229 Aliyah Harbors\nSoniafurt, CT 89994-9195',NULL,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(5,'Alexzander Walter',5,'crooks.cordell@example.com',NULL,'734-232-4941','$2y$10$kyQg2ZmWkAzPk/bPMCdivues95n3371HbpgG6WPZ8ONgbZUMlEQxC','583 Hintz Fork Suite 153\nOsinskibury, NJ 47980-1419',NULL,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(6,'Geovanni Schaefer',1,'wendy30@example.net',NULL,'1-962-283-5810 x260','$2y$10$lln3PS8t/Egt7jNa.L4zVuYs1ESZFzF0OeQY4/a9AOyiNRNJYp8Cy','496 Buddy Mission\nOmaside, DE 41172',NULL,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(7,'Ahmad Torp',2,'thiel.glenna@example.com',NULL,'745-556-1356','$2y$10$YXxAYjQoqEI9A2cvehcU2.U0Yho.dSRMvq8cxY7feYwpRckdLcqMK','77515 Vandervort Spur Apt. 001\nLake Jayce, KS 98074',NULL,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(8,'Stan Schulist',3,'maci11@example.com',NULL,'(896) 310-9672','$2y$10$BSs/rfk5tZM8pEkpaUX6Gu6Ab756aUa7IFvvXZJ.maFo2d7d2lC.O','51460 Watsica Ways Apt. 367\nNorth Oswaldo, MN 84714-9596',NULL,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(9,'Bennie Labadie',1,'igutkowski@example.org',NULL,'+1.802.847.3777','$2y$10$DlATSyleX9HXCVHYXqxa0uIkaRf9qUgXuWLtr8JqUg.q75DbX4ZP.','644 Eldred Drive Apt. 179\nHartmannhaven, AR 14417',NULL,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(10,'Camren Carter',3,'carole.mcdermott@example.net',NULL,'+1-328-968-6884','$2y$10$ky9qoAZDbjPtHVWVetaWFOf5bNkdToMQTnPWlL.KCeSLFI0Tz02d.','107 Smith Roads Apt. 753\nNew Adolftown, KS 10093',NULL,'2020-03-27 12:38:32','2020-03-27 12:38:32'),(11,'Habibur Rahman Anik',1,'anik@gmail.com','{\"id\":4,\"group_name\":\"Repellat deleniti aspernatur optio earum.\",\"semester_id\":1,\"students_info\":\"Illo ex itaque et nulla est nam ipsa. Modi et sapiente fuga autem doloremque deserunt.\",\"deta','01711111111','$2y$10$84iWSvIlWiHUBaUV4WssoOpHVuyhYEMdu05vRNFguAfVn1yfOqVji','',NULL,NULL,NULL),(12,'Shapla Akter',1,'shapla@gmail.com','{\"id\":1,\"group_name\":\"Illum error consectetur omnis exercitationem.\",\"semester_id\":1,\"students_info\":\"Ea impedit et et quia veritatis. Possimus sint dolorem maiores et. Minus ipsam qui illum ','017100000000','$2y$10$ozruWquWgUdPKAqIxWjVDermDP3gEN78b9BboR1I3Fe5LKw7pLa.a','',NULL,NULL,NULL);
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher_password_resets`
--

DROP TABLE IF EXISTS `teacher_password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `teacher_password_resets_email_index` (`email`),
  KEY `teacher_password_resets_token_index` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher_password_resets`
--

LOCK TABLES `teacher_password_resets` WRITE;
/*!40000 ALTER TABLE `teacher_password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `teacher_password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teachers`
--

DROP TABLE IF EXISTS `teachers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teachers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `initial` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_id` bigint(20) unsigned DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `specialization` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_supervisor` tinyint(4) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `teachers_email_unique` (`email`),
  UNIQUE KEY `teachers_mobile_unique` (`mobile`),
  KEY `teachers_department_id_foreign` (`department_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teachers`
--

LOCK TABLES `teachers` WRITE;
/*!40000 ALTER TABLE `teachers` DISABLE KEYS */;
INSERT INTO `teachers` VALUES (1,'Cornell Steuber','SSK',3,'esther.herzog@example.com','337-623-2205','$2y$10$Ad0iG/V9CtI.hliHaRxTU.hOa7sL1iUcJyKiBbXp25uaiPx069aPC','50590 Sebastian Skyway\nNorth Zoilahaven, TN 85300-3054',NULL,'Machine Learning',0,NULL,'2020-03-27 12:38:31','2020-03-27 12:38:31'),(2,'Mitchell Barton','PPD',1,'evert73@example.org','847.878.8742 x199','$2y$10$akzdiSLPZYuUnWYDp.WG1e.LIpJuhTitUrr2LELAHPiZlbXrKOhaC','73693 Maggio Meadow Apt. 548Dedrickchester, TN 13913',NULL,'Data Analysis',0,NULL,'2020-03-27 12:38:31','2020-03-28 07:51:52'),(3,'Chance Gleichner','SSK',2,'juana.mraz@example.org','(474) 658-3171','$2y$10$GIfFPC/AHnPbMT7GgraBSuyhGsQMHUTKtnsmLKljqRuR1oywsxymi','9886 Boyle GreenWest Toy, SC 01828',NULL,'Machine Learning',0,NULL,'2020-03-27 12:38:31','2020-03-28 07:51:47'),(4,'Nigel Ziemann','WWE',1,'candida.leffler@example.net','+1 (921) 815-1547','$2y$10$Vgjtm2fjZEuJNMEyz5xGdecM1IvpII7WDkDPKhlRoQnWq13TdeFpa','122 Eusebio Run Apt. 308\nSouth Rae, WY 04128',NULL,'Database',0,NULL,'2020-03-27 12:38:31','2020-03-27 12:38:31'),(5,'Jerry Upton','KKR',1,'kessler.maribel@example.org','575-913-9729 x64090','$2y$10$ldkQu8nygQKFC3DbJsIH2.ai4yyRQrHSCH3SmLiuLm48/o2KzNwyK','80286 Reynolds Wells\nNew Brooks, NV 83650',NULL,'Algorithm',1,NULL,'2020-03-27 12:38:31','2020-03-27 12:38:31'),(6,'Mahfujur Rahman Raju','MRR',1,'teacher1@gmail.com','0175555555','$2y$10$oakUJMgqm2B4.rhSkZ1MOOOZZR/vlXtzr2ieneV.iyBELKjCfBupa',NULL,NULL,'Database',0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `teachers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'shapla'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-29  1:45:15
