<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use App\Models\Assign;
use App\Models\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;

class AssignController extends Controller
{
    public function assigned_group(){
        $data = [
            'groups' => Group::with('semester','interest','teacher')
                ->where('teacher_id',Auth::guard('teacher')->user()->id)
                ->orderBy('id', 'desc')->get()
        ];

        return view('teacher.assign.index')->with($data);
    }

    public function view_assigned ($group_id){
        $data =   [
            'assigns' => DB::table('assigns')
                ->join('students','students.id','=','assigns.student_id')
                ->join('teachers','teachers.id','=','assigns.teacher_id')
                ->join('groups','groups.group_id','=','students.group_id')
                ->join('departments','departments.id','=','students.department_id')
                ->select('students.*','departments.department_name','assigns.created_at')
                ->where(
                    [
                        'assigns.teacher_id' => Auth::guard('teacher')->user()->id,
                        'groups.group_id' => $group_id
                    ]
                )
            ->get(),
            'group_id' => $group_id
        ];
        //return $data;
        return view('teacher.assign.view_assigned')->with($data);
    }



    public function delete_group($id) {
        $assign = Assign::find($id);
        if ($assign->delete()) {
            Session::flash('success', 'Assign deleted successfully');
            return redirect('teacher/assign/group');
        } else {
            Session::flash('success', 'Assign update failed');
            return redirect('teacher/assign/group');
        }
    }

}
