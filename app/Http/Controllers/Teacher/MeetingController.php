<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;
use App\Models\Meeting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use Session;

class MeetingController extends Controller
{
    public function index()
    {
        $data =   [
            'meetings' => Meeting::with(['teacher'])->where('teacher_id',Auth::guard('teacher')->user()->id)->orderBy('id', 'desc')->get()
        ];
        return view('teacher.meeting.index')->with($data);
    }

    public function create()
    {
        $data =   [
            'days' => [
                'Saturday','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday'
            ]
        ];
        return view('teacher.meeting.create')->with($data);
    }

    public function store(Request $request)
    {
        $data['date'] = $request->date;
        $data['time'] = $request->hour.':'.$request->minute.' '.$request->amorpm;
        $data['teacher_id'] = $this->teacher()->id;
        if (Meeting::create($data)) {
            Session::flash('success', 'Meeting Added successfully!');
            return redirect(route('teacher.meeting.index'));
        } else {
            Session::flash('error', 'Something Went wrong!');
            return redirect(route('teacher.meeting.create'));
        }
    }

    public function edit(Meeting $student)
    {
        $data =   [
            'student' => $student,
            'departments' => Department::orderBy('department_name')->get()
        ];
        return view('teacher.meeting.edit')->with($data);
    }

    public function update(Request $request, Meeting $student)
    {

        $student->name = $request->name;
        $student->department_id = $request->department_id;
        $student->email = $request->email;
        $student->mobile = $request->mobile;
        $student->address = $request->address;
        if (!empty($request->password))
        {
            $student->password = Hash::make($request->password);
        }
        $student->updated_at = now();
        if ($student->update()) {
            Session::flash('success', 'Meeting updated successfully');
            return redirect(route('teacher.meeting.index'));
        } else {
            Session::flash('error', 'Failed to update vendor');
            return redirect(route('teacher.meeting.index'));
        }
    }

    public function destroy(Meeting $student)
    {
        if ($student->delete()) {
            Session::flash('success', 'Meeting deleted successfully');
            return redirect(route('teacher.meeting.index'));
        } else {
            Session::flash('success', 'Meeting update failed');
            return redirect(route('teacher.meeting.index'));
        }
    }

    private function validateRequest()
    {
        return request()->validate([
            'name' => 'required|min:6',
            'phone' => 'required',
            'email' => 'required',
            'location' => 'required'
        ]);
    }

    private function teacher()
    {
        return Auth::guard('teacher')->user();
    }
}
