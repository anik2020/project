<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Researchwork;
use Illuminate\Http\Request;

class ResearchController extends Controller
{
    public function index()
    {
        $data =   [
            'researches' => Researchwork::with('teacher','group')->where('status',2)->get()
        ];

        return view('front.research.index')->with($data);
    }


    public function previous()
    {
        $data =   [
            'researches' => Researchwork::with('teacher','group')->where('status',3)->get()
        ];

        return view('front.research.previous')->with($data);
    }




}
