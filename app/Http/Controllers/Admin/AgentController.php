<?php

namespace App\Http\Controllers\Admin;

use Session;
use App\Models\Agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Zone;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Admin\MailController;

class AgentController extends Controller
{
    // view  all agents
    public function index()
    {
        $data =   [
            'agents' => Agent::with(['zone'])->orderBy('name', 'asc')->get()

        ];

        return view('admin.agent.index')->with($data);
    }


    //create agent form
    public function create()
    {
        $data =   [
            'zones' => Zone::orderBy('name', 'asc')->get()
        ];


        return view('admin.agent.create')->with($data);
    }

    // store/save agent
    public function store(Request $request)
    {
        $agentData  = $this->validateRequest();
        $agentData['created_at'] = now();
        $agentData['password'] = Hash::make($request->password);
        if (Agent::create($agentData)) {

            MailController::send_mail(
                [
                    'view' => 'Login',
                    'link' => url('agent'),
                    'name' => $request->name,
                    'username' => $request->email,
                    'password' => $request->password,
                ],
                'Account Confirmation - Chahida.com.bd',
                $request->email,
                'admin.mail.account.account_confirmation'
            );
            Session::flash('success', 'Agent Added successfully!');
            return redirect(route('admin.agent.index'));
        } else {
            Session::flash('error', 'Something Went wrong!');
            return redirect(route('admin.agent.create'));
        }
    }


    //edit agent
    public function edit($id)
    {
        $data =   [
            'agent' => Agent::find($id),
            'zones' => Zone::orderBy('name', 'asc')->get()
        ];

        return view('admin.agent.edit')->with($data);
    }


    //update agent
    public function update(Request $request, Agent $agent)
    {
        $agent->name = $request->name;
        $agent->phone = $request->phone;
        $agent->zone_id = $request->zone_id;
        $agent->address = $request->address;
        if (!empty($request->password)) {
            $agent->password = Hash::make($request->password);
        }
        $agent->updated_at = now();
        if ($agent->save()) {
            Session::flash('success', 'Agent updated successfully');
            return redirect(route('admin.agent.index'));
        } else {
            Session::flash('error', 'Failed to update vendor');
            return redirect(route('admin.agent.index'));
        }
    }


    //delete agent
    public function destroy(Agent $agent)
    {
        if ($agent->delete()) {
            Session::flash('success', 'Agent deleted successfully');
            return redirect(route('admin.agent.index'));
        } else {
            Session::flash('success', 'Agent update failed');
            return redirect(route('admin.agent.index'));
        }
    }

    //validation data
    private function validateRequest()
    {
        return request()->validate([
            'name' => 'required|min:3',
            'password' => 'required|min:6',
            'phone' => 'required|unique:agents',
            'email' => 'required|email|unique:agents,email,$this->id,id',
            'email' => 'required|min:3|max:150',
            'zone_id' => 'required',
            'address' => 'sometimes',
        ]);
    }
}