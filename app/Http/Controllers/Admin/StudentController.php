<?php

namespace App\Http\Controllers\Admin;

use App\Models\Department;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Session;

class StudentController extends Controller
{
    public function index()
    {

        $data =   [
            'students' => Student::with(['department'])->orderBy('name', 'asc')->get()
        ];

        return view('admin.student.index')->with($data);
    }

    public function create()
    {
        $data =   [
            'departments' => Department::orderBy('department_name')->get()
        ];
        return view('admin.student.create')->with($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if (!empty($request->password))
        {
            $data['password'] = Hash::make($request->password);
        }
        if (Student::create($data)) {
            Session::flash('success', 'Student Added successfully!');
            return redirect(route('admin.student.index'));
        } else {
            Session::flash('error', 'Something Went wrong!');
            return redirect(route('admin.student.create'));
        }
    }

    public function edit(Student $student)
    {
        $data =   [
            'student' => $student,
            'departments' => Department::orderBy('department_name')->get()
        ];
        return view('admin.student.edit')->with($data);
    }

    public function update(Request $request, Student $student)
    {

        $student->name = $request->name;
        $student->department_id = $request->department_id;
        $student->email = $request->email;
        $student->mobile = $request->mobile;
        $student->address = $request->address;
        if (!empty($request->password))
        {
            $student->password = Hash::make($request->password);
        }
        $student->updated_at = now();
        if ($student->update()) {
            Session::flash('success', 'Student updated successfully');
            return redirect(route('admin.student.index'));
        } else {
            Session::flash('error', 'Failed to update vendor');
            return redirect(route('admin.student.index'));
        }
    }

    public function destroy(Student $student)
    {
        if ($student->delete()) {
            Session::flash('success', 'Student deleted successfully');
            return redirect(route('admin.student.index'));
        } else {
            Session::flash('success', 'Student update failed');
            return redirect(route('admin.student.index'));
        }
    }

    private function validateRequest()
    {
        return request()->validate([
            'name' => 'required|min:6',
            'phone' => 'required',
            'email' => 'required',
            'location' => 'required'
        ]);
    }
}
