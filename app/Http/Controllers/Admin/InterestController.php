<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Interest;
use App\Models\Semester;
use App\Models\Teacher;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Session;

class InterestController extends Controller
{
    /**
     * @param Request $request
     * @return Factory|RedirectResponse|Redirector|View
     */
    public function choice(Request $request)
    {
        if ($request->isMethod('POST')) {

            foreach ($request->all()['teacher_id'] as $value) {
                $interest = new Interest;
                $interest->teacher_id = $value;
                $interest->student_id = $this->getUser()->id;
                $interest->save();
            }
            Session::flash('success', 'You have added ' . count($request->all()['teacher_id']) . ' teachers in your choice list');
            return redirect('admin/interest/choice_list');
        }

        $data = [
            'teachers' => Teacher::orderBy('name')->get(),
            'semesters' => Semester::orderBy('semester_name')->get()
        ];
        return view('admin.interest.choice')->with($data);
    }

    /**
     * @return Factory|View
     */
    public function choice_list()
    {
        $data = [

            'interests' => Interest::with(['teacher', 'student'])->where([
                'status' => 0
            ])->get()
        ];
        return view('admin.interest.choice_list')->with($data);
    }

    /**
     * @return mixed
     */
    private function getUser()
    {
        return Auth::guard('student')->user();
    }
}
