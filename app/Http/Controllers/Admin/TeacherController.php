<?php

namespace App\Http\Controllers\Admin;

use App\Models\Department;
use App\Models\Teacher;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;
use Session;

class TeacherController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {
        $data =   [
            'teachers' => Teacher::with(['department'])->orderBy('name', 'asc')->get()
        ];

        return view('admin.teacher.index')->with($data);
    }
    public function supervisor_index()
    {
        $data =   [
            'teachers' => Teacher::with(['department'])->where('is_supervisor', 1)->orderBy('name', 'asc')->get()
        ];
        return view('admin.teacher.supervisor_index')->with($data);
    }

    /**
     * @return Factory|View
     */
    public function create()
    {
        $data =   [
            'departments' => Department::orderBy('department_name')->get()
        ];
        return view('admin.teacher.create')->with($data);
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
        $data = $request->all();

        if (!empty($request->password)) {
            $data['password'] = Hash::make($request->password);
        }
        if (Teacher::create($data)) {
            Session::flash('success', 'Teacher Added successfully!');
            return redirect(route('admin.teacher.index'));
        } else {
            Session::flash('error', 'Something Went wrong!');
            return redirect(route('admin.teacher.create'));
        }
    }

    public function edit(Teacher $teacher)
    {
        $data =   [
            'teacher' => $teacher,
            'departments' => Department::orderBy('department_name')->get()
        ];
        return view('admin.teacher.edit')->with($data);
    }


    /**
     * @param Request $request
     * @param Teacher $teacher
     * @return RedirectResponse|Redirector
     */
    public function update(Request $request, Teacher $teacher)
    {
        $teacher->name = $request->name;
        $teacher->initial = $request->initial;
        $teacher->department_id = $request->department_id;
        $teacher->mobile = $request->mobile;
        $teacher->address = $request->address;
        $teacher->is_supervisor = $request->is_supervisor;
        if (!empty($request->password)) {
            $teacher->password = Hash::make($request->password);
        }
        $teacher->updated_at = now();
        if ($teacher->update()) {
            Session::flash('success', 'Teacher updated successfully');
            return redirect(route('admin.teacher.index'));
        } else {
            Session::flash('error', 'Failed to update vendor');
            return redirect(route('admin.teacher.index'));
        }
    }

    /**
     * @param Teacher $teacher
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function destroy(Teacher $teacher)
    {
        if ($teacher->delete()) {
            Session::flash('success', 'Teacher deleted successfully');
            return redirect(route('admin.teacher.index'));
        } else {
            Session::flash('success', 'Teacher update failed');
            return redirect(route('admin.teacher.index'));
        }
    }

    /**
     * vaildata form data
     * @return array
     */
    private function validateRequest()
    {
        return request()->validate([
            'name' => 'required|min:6',
            'phone' => 'required',
            'email' => 'required',
            'location' => 'required'
        ]);
    }
}
