<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Researchwork;
use App\Models\Student;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Session;

class ResearchController extends Controller
{
    public function index()
    {
        $data =   [
            'researches' => Researchwork::with('teacher','group')->get()
        ];

        return view('admin.research.index')->with($data);
    }

    public function create()
    {
        $data =   [
            'teachers' => Teacher::orderBy('name')->get(),
            'students' => Student::orderBy('name')->get(),
        ];
        return view('admin.research.create')->with($data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['status']  = 1;

        if (Researchwork::create($data)) {
            Session::flash('success', 'Assign Added successfully!');
            return redirect(route('admin.research.index'));
        } else {
            Session::flash('error', 'Something Went wrong!');
            return redirect(route('admin.research.create'));
        }
    }

    public function edit(Assign $group)
    {
        $data =   [
            'group' => $group,
        ];

        return view('admin.research.edit')->with($data);
    }

    public function update(Request $request, Assign $group)
    {

        $group->group_id = $request->group_id;

        $group->updated_at = now();
        if ($group->update()) {
            Session::flash('success', 'Assign updated successfully');
            return redirect(route('admin.research.index'));
        } else {
            Session::flash('error', 'Failed to update vendor');
            return redirect(route('admin.research.index'));
        }
    }

    public function destroy(Assign $group)
    {
        if ($group->delete()) {
            Session::flash('success', 'Assign deleted successfully');
            return redirect(route('admin.research.index'));
        } else {
            Session::flash('success', 'Assign update failed');
            return redirect(route('admin.research.index'));
        }
    }

}
