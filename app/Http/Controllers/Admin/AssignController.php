<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\HelperController;
use App\Models\Assign;
use App\Models\Interest;
use App\Models\Student;
use App\Models\Group;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Session;

class AssignController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = [
            'assigns' => Assign::with('teacher', 'student')->get()
        ];

        return view('admin.assign.index')->with($data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $data = [
            'interests' => Interest::orderBy('id')->where('status', 0)->get(),
            'teachers' => Teacher::orderBy('name', 'asc')->get(),
        ];

        return view('admin.assign.create')->with($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['status'] = 1;

        if (Assign::create($data)) {
            Session::flash('success', 'Assign Added successfully!');
            return redirect(route('admin.assign.index'));
        } else {
            Session::flash('error', 'Something Went wrong!');
            return redirect(route('admin.assign.create'));
        }
    }

    /**
     * @param Assign $group
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Assign $group)
    {
        $data = [
            'group' => $group,
        ];

        return view('admin.assign.edit')->with($data);
    }

    /**
     * @param Request $request
     * @param Assign $group
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Assign $group)
    {

        $group->group_id = $request->group_id;

        $group->updated_at = now();
        if ($group->update()) {
            Session::flash('success', 'Assign updated successfully');
            return redirect(route('admin.assign.index'));
        } else {
            Session::flash('error', 'Failed to update vendor');
            return redirect(route('admin.assign.index'));
        }
    }

    /**
     * @param Assign $group
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Assign $group)
    {
        if ($group->delete()) {
            Session::flash('success', 'Assign deleted successfully');
            return redirect(route('admin.assign.index'));
        } else {
            Session::flash('success', 'Assign update failed');
            return redirect(route('admin.assign.index'));
        }
    }

}
