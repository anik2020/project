<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\Interest;
use App\Models\Semester;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use Session;

class GroupController extends Controller
{
    /**
     * @return Factory|View
     */
    public function index()
    {

        $data = [
            'groups' => Group::with('semester','interest','teacher')
                ->where('status',0)
                ->orderBy('id', 'desc')->get()
        ];
//        return $data['groups'];
        return view('admin.group.index')->with($data);
    }

    /**
     * @return Factory|View
     */
    public function create()
    {
        $data = [
            'semesters' => Semester::orderBy('semester_name')->get()
        ];

        return view('admin.group.create')->with($data);
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Redirector
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        if (Group::create($this->validation($request))) {
            Session::flash('success', 'Group Added successfully!');
            return redirect(route('admin.group.index'));
        } else {
            Session::flash('error', 'Something Went wrong!');
            return redirect(route('admin.group.create'));
        }
    }

    /**
     * @param Group $group
     * @return Factory|View
     */
    public function edit(Group $group)
    {
        $data = [
            'group' => $group,
            'semesters' => Semester::orderBy('semester_name')->get()
        ];

        return view('admin.group.edit')->with($data);
    }

    /**
     * @param Request $request
     * @param Group $group
     * @return RedirectResponse|Redirector
     */
    public function update(Request $request, Group $group)
    {
        $group->group_name = $request->group_name;
        $group->semester_id = $request->semester_id;
        $group->students_info = $request->students_info;
        $group->details = $request->details;
        $group->updated_at = now();
        if ($group->update()) {
            Session::flash('success', 'Group updated successfully');
            return redirect(route('admin.group.index'));
        } else {
            Session::flash('error', 'Failed to update vendor');
            return redirect(route('admin.group.index'));
        }
    }

    /**
     * @param Group $group
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function destroy(Group $group)
    {
        if ($group->delete()) {
            Session::flash('success', 'Group deleted successfully');
            return redirect(route('admin.group.index'));
        } else {
            Session::flash('success', 'Group update failed');
            return redirect(route('admin.group.index'));
        }
    }


    /**
     * save interest group in group table
     * @param Request $request
     */
    public function save_interest_group(Request $request)
    {
        for ($i = 0; $i < count($request->teacher_id); $i++) {
            $group = new Group();
            $group->teacher_id = $request->teacher_id[$i];
            $group->students_info = $request->students_info[$i];
            $group->interest_id = $request->interest_id[$i];
            $group->semester_id = $request->semester_id[$i];
            $group->save();

            $interest = Interest::find($request->interest_id[$i]);
            $interest->status = 1;
            $interest->save();
        }

        Session::flash('success', 'Assigned succcesfully');
        return redirect()->back();

    }


    /**
     * @param Request $request
     * @return array
     */
    private function validation($request)
    {
        return $request->validate([
            'group_name' => 'required|unique:groups|min:3|max:255',
            'semester_id' => 'required',
            'students_info' => 'required',
            'details' => 'required',
        ]);
    }

}
