<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\NoticeCategory;
use Illuminate\Http\Request;
use App\Models\Notice;

class NoticeController extends Controller
{
    public function index()
    {
        $data =   [
            'notices' => Notice::with('notice_category')->get()
        ];


        return view('admin.notice.index')->with($data);
    }


    public function create()
    {
        return view('admin.notice.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        NoticeCategory::create($data);
        Session::flash('success', 'Category inserted successful');
        return redirect(route('admin.notice.index'));
    }




    public function edit(Request $request,$id)
    {
        $data = [

            'notice_category' => NoticeCategory::find($id)
        ];

        return view('admin.notice.edit')->with($data);
    }


    public function update(Request $request, $id)
    {
        $category = NoticeCategory::find($id);
        $category->category_name = $request->category_name;

        if ($category->save()) {
            # code...
            Session::flash('success', 'Category updated successfully');
            return redirect(route('admin.notice.index'));
        } else {
            Session::flash('error', 'Category update failed');
            return redirect(route('admin.notice.index'));
        }
    }


    public function destroy($id)
    {
        $category = NoticeCategory::find($id);
        if ($category->delete()) {
            # code...
            Session::flash('success', 'Notice Category deleted successfully');
            return redirect(route('admin.notice.index'));
        } else {
            Session::flash('error', 'Notice Category deleted failed');
            return redirect(route('admin.notice.index'));
        }
    }
}
