<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Semester;
use Illuminate\Http\Request;
use Session;

class SemesterController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = [
            'semesters' => Semester::all()
        ];

        return view('admin.semester.index')->with($data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $data = [

        ];
        return view('admin.semester.create')->with($data);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $this->validation($request);
        if (Semester::create($data)) {
            Session::flash('success', 'Group Added successfully!');
            return redirect(route('admin.semester.index'));
        } else {
            Session::flash('error', 'Something Went wrong!');
            return redirect(route('admin.semester.create'));
        }
    }

    /**
     * @param Semester $semester
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Semester $semester)
    {
        $data = [
            'semester' => $semester,
        ];

        return view('admin.semester.edit')->with($data);
    }

    /**
     * @param Request $request
     * @param Semester $semester
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Semester $semester)
    {

        $semester->semester_name = $request->semester_name;
        $semester->updated_at = now();
        if ($semester->update()) {
            Session::flash('success', 'Group updated successfully');
            return redirect(route('admin.semester.index'));
        } else {
            Session::flash('error', 'Failed to update vendor');
            return redirect(route('admin.semester.index'));
        }
    }

    /**
     * @param Semester $semester
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Semester $semester)
    {
        if ($semester->delete()) {
            Session::flash('success', 'Group deleted successfully');
            return redirect(route('admin.semester.index'));
        } else {
            Session::flash('success', 'Group update failed');
            return redirect(route('admin.semester.index'));
        }
    }

    /**
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     */
    private function validation($request)
    {
       return $request->validate([
            'semester_name' => 'required|unique:semesters|min:3|max:255'
        ]);
    }
}
