<?php

namespace App\Http\Controllers\Student;

use App\Models\Assign;
use App\Models\Department;
use App\Models\Group;
use App\Models\Interest;
use App\Models\Teacher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;

class TeacherController extends Controller
{
    /*show teacher list*/
    public function index()
    {
        $data =   [
            'teachers' => Teacher::with(['department'])->orderBy('name', 'asc')->get(),
            'assigns' => Assign::with('teacher')->where('student_id',Auth::guard('student')->user()->id)->get()
        ];

//        return $data['assigns'];
        return view('student.teacher.index')->with($data);
    }

    public function assigned()
    {
        $data = [
            'assigns' => Interest::with(['group','teacher'])
                ->where([
                    'student_id' => Auth::guard('student')->user()->id
                ])
                ->get()
        ];


        return view('student.teacher.assigned_teacher')->with($data);
    }


}
