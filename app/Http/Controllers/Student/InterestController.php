<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\Interest;
use App\Models\Semester;
use App\Models\Teacher;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Session;

class InterestController extends Controller
{
    /**
     * @param Request $request
     * @return Factory|RedirectResponse|Redirector|View
     */
    public function choice(Request $request)
    {
        if ($request->isMethod('POST')) {

            $data = $request->all();
            $teacher_id = '';

            for ($i = 0; $i < count($data['teacher_id']); $i++) {

                $teacher_id .= $data['teacher_id'][$i].',';

            }
            $teacher_id = rtrim($teacher_id, ',');

            $interest = new Interest;
            $interest->semester_id = $data['semester_id'];
            $interest->students_name = $data['students_name'];
            $interest->student_id = Auth::guard('student')->user()->id;
            $interest->students_id = $data['students_id'];
            $interest->area_of_research = $data['area_of_research'];
            $interest->tentative_title = $data['tentative_title'];
            $interest->teacher_id = $teacher_id;
            $interest->save();
            Session::flash('success', 'You have added ' . count($request->all()['teacher_id']) . ' teachers in your choice list');
            return redirect('student/interest/choice_list');
        }

        $data = [
            'semesters' => Semester::orderBy('semester_name')->get(),
            'teachers' => Teacher::orderBy('name')->get(),
        ];
        return view('student.interest.choice')->with($data);
    }

    /**
     * @return Factory|View
     */
    public function choice_list()
    {
        $data = [
            'interests' => Interest::with('semester')->where([
                'student_id'=> $this->getUser()->id,
                'status' => 0
            ])->get()
        ];

        return view('student.interest.choice_list')->with($data);
    }

    private function getUser()
    {
        return Auth::guard('student')->user();
    }
}
