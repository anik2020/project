<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use App\Models\Meeting;
use App\Models\StudentMeeting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MeetingController extends Controller
{
    /*this is for showing student meeting list*/
    public function index()
    {
        //yes
        //return Auth::guard('student')->user();

        $data =   [
            'meetings' => DB::table('student_meetings')
                ->join('meetings','student_meetings.id','=','meetings.id')
                ->join('teachers','teachers.id','=','meetings.teacher_id')
                ->select('teachers.name','meetings.date','meetings.time','meetings.status')
                ->where('student_meetings.student_id',Auth::guard('student')->user())
                ->get()
        ];
        return view('student.meeting.index')->with($data);
    }
}
