<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Teacher;

class HelperController extends Controller
{
    /**
     * get Teacher Initials according to Teacher ID
     * @param string $data
     * @return array
     */
    public static function getTeacherInitials($data)
    {
        $array = explode(',', $data);
        $teachers = Teacher::select('id', 'initial')->get();
        $new_teacher = [];

        foreach ($teachers as $teacher) {
            if (in_array($teacher->id, $array)) {
                array_push($new_teacher, $teacher->initial);
            }
        }
        foreach ($new_teacher as $item) {
            echo $item . ',';
        }
    }

    /**
     * get Teacher Initials according to Teacher ID
     * @param string $data
     * @return array
     */
    public static function getTeacherInitialswithOptions($data)
    {
        return $array = explode(',', $data);
    }


}
