<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Group extends Model
{
    protected $fillable = [
      'group_name','semester_id','students_info','details'
    ];

    public function researchwork()
    {
        return $this->belongsTo(Researchwork::class);
    }


    /**
     * @return BelongsTo
     */
    public function semester()
    {
        return $this->belongsTo(Semester::class);
    }

    /**
     * @return BelongsTo
     */
    public function interest()
    {
        return $this->belongsTo(Interest::class);
    }

    /**
     * @return BelongsTo
     */
    public function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }





}
