<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NoticeCategory extends Model
{
    protected  $fillable = [
        'category_name'
    ];

    public function notices()
    {
        return $this->hasMany(Notice::class);
    }
}
