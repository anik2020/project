<?php

use App\Models\Admin;
use App\Models\Assign;
use App\Models\Department;
use App\Models\Group;
use App\Models\Interest;
use App\Models\Meeting;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\Semester;
use App\Models\StudentMeeting;
use App\Models\Researchwork;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        factory(Admin::class, 1)->create();
        factory(Semester::class, 1)->create();
        factory(Group::class, 4)->create();
        factory(Department::class, 5)->create();
        factory(Teacher::class, 5)->create();
        factory(Student::class, 10)->create();
        factory(Meeting::class, 10)->create();
        factory(StudentMeeting::class, 10)->create();
        factory(Assign::class, 10)->create();
        factory(Researchwork::class, 10)->create();
        factory(\App\Models\NoticeCategory::class, 5)->create();
        factory(\App\Models\Notice::class, 10)->create();
        factory(Interest::class, 5)->create();


        //insert data to students table
        DB::table('teachers')->insert([
            [
                'name' => 'Mahfujur Rahman Raju',
                'initial' => 'MRR',
                'email' => 'raju@gmail.com',
                'department_id' => Department::first()->id,
                'mobile' => '0175555555',
                'password' => Hash::make(123)
            ],
        ]);

        //insert data to students table
        DB::table('students')->insert([
            ['name' => 'Habibur Rahman Anik',
                'email' => 'anik@gmail.com',
                'department_id' => Department::first()->id,
                'group_id' => Group::all()->random(),
                'mobile' => '01711111111',
                'password' => Hash::make(123)
            ],
            [
                'name' => 'Shapla Akter',
                'email' => 'shapla@gmail.com',
                'department_id' => Department::first()->id,
                'group_id' => Group::all()->random(),
                'mobile' => '017100000000',
                'password' => Hash::make(123)
            ],
        ]);

        //insert data to students table
        DB::table('semesters')->insert([
            [
                'semester_name' => 'Spring 2019',
            ],
            [
                'semester_name' => 'Fall 2019',
            ],
            [
                'semester_name' => 'Summer 2019',
            ],
            [
                'semester_name' => 'Spring 2020',
            ],
            [
                'semester_name' => 'Summer 2020',
            ],
            [
                'semester_name' => 'Fall 2020',
            ]
        ]);
    }
}
