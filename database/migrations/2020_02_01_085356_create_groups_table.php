<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupsTable extends Migration
{

    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('teacher_id ')->nullable();
            $table->unsignedBigInteger('interest_id ')->nullable();
            $table->unsignedBigInteger('semester_id')->nullable();
            $table->string('students_info', 199);
            $table->text('details');
            $table->tinyInteger('status')->default(0);
            $table->foreign('interest_id')->references('id')->on('interests')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('semester_id')->references('id')->on('semesters')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('teacher_id')->references('id')->on('teachers')->onDelete('set null')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
