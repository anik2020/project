<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeetingtimesTable extends Migration
{

    public function up()
    {
        Schema::create('meetingtimes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('day');
            $table->string('start_time');
            $table->string('end_time');
            $table->unsignedBigInteger('teacher_id');
            $table->string('status')->default(0);
            $table->timestamps();
            $table->foreign('teacher_id')->references('id')->on('teachers')->onDelete('cascade')->onUpdate('cascade');
        });
    }


    public function down()
    {
        Schema::dropForeign('teacher_id');
        Schema::dropIfExists('meetingtimes');
    }
}