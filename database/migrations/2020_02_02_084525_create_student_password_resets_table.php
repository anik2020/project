<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentPasswordResetsTable extends Migration
{
    public function up()
    {
        Schema::create('student_password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token')->index();
            $table->timestamp('created_at')->nullable();

        });
    }

    public function down()
    {
        Schema::drop('student_password_resets');
    }
}