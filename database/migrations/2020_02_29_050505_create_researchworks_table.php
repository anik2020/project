<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResearchworksTable extends Migration
{

    public function up()
    {
        Schema::create('researchworks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('topic');
            $table->longText('description')->nullable();
            $table->unsignedBigInteger('group_id')->nullable();
            $table->unsignedBigInteger('teacher_id')->nullable();
            $table->tinyInteger('status')->default(1);
            //TODO:: 1 for pending
            //TODO:: 2 for running
            //TODO:: 3 for completed
            $table->timestamps();
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('teacher_id')->references('id')->on('teachers')->onDelete('set null')->onUpdate('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('researchworks');
    }
}