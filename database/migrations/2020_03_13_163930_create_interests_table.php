<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInterestsTable extends Migration
{


    public function up()
    {
        Schema::create('interests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('semester_id')->nullable();
            $table->string('teacher_id',20);
            $table->string('students_name');
            $table->string('student_id');
            $table->string('students_id');
            $table->text('area_of_research');
            $table->string('tentative_title');
            $table->tinyInteger('status')->default(0);
            $table->foreign('semester_id')->references('id')->on('semesters')->onDelete('set null')->onUpdate('cascade');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('interests');
        Schema::dropForeign('semester_id');
    }
}
