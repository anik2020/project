<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoticesTable extends Migration
{

    public function up()
    {
        Schema::create('notices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->unsignedBigInteger('notice_category_id');
            $table->longText('description');
            $table->string('attach')->nullable();

            $table->foreign('notice_category_id')->references('id')->on('notice_categories')->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('notices');
    }
}