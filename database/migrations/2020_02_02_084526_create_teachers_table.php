<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeachersTable extends Migration
{

    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('initial');
            $table->unsignedBigInteger('department_id')->nullable();
            $table->string('email')->unique();
            $table->string('mobile')->unique()->nullable();
            $table->string('password');
            $table->string('address')->nullable();
            $table->string('image')->nullable();
            $table->string('specialization')->nullable();
            $table->tinyinteger('is_supervisor')->default(0);
            $table->rememberToken();
            $table->foreign('department_id')->references('id')->on('departments')->onDelete('set null')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('teachers');
        Schema::dropForeign('department_id');
    }
}
