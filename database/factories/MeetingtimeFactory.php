<?php



use App\Models\Teacher;
use App\Models\Meetingtime;
use Faker\Generator as Faker;

$factory->define(Meetingtime::class, function (Faker $faker) {

    return [
        'day' => $faker->date('D'),
        'start_time' => $faker->time('h:i:sA'),
        'end_time' => $faker->time('h:i:sA'),
        'teacher_id' => Teacher::all()->random()
    ];
});