<?php

/** @var Factory $factory */

use App\Models\Interest;
use App\Models\Student;
use App\Models\Teacher;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Interest::class, function (Faker $faker) {
    return [
        'teacher_id' => Teacher::all()->random(),
        'students_name' => Student::all()->random()->name .
            ','
            . Student::all()->random()->name.
            ',' .
        Student::all()->random()->name
        ,
        'students_id' => $faker->randomElement(['127', '173', '164', '237', '233', '987']) .
            ','
            . $faker->randomElement(['127', '173', '164', '237', '233', '987'])
        ,
        'area_of_research' => $faker->randomElement(['Zoology', 'Biology', 'Pharmacy', 'Data Mining', 'Machine Learning']),
        'tentative_title' => ucfirst($faker->text(100))
    ];
});

