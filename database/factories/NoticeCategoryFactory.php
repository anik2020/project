<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\NoticeCategory;
use Faker\Generator as Faker;

$factory->define(NoticeCategory::class, function (Faker $faker) {
    return [
        'category_name'=> ucfirst($faker->text(20))
    ];
});
