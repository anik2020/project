<?php


use App\Models\Group;
use Faker\Generator as Faker;

$factory->define(Group::class, function (Faker $faker) {
    return [
        'group_name' => ucfirst($faker->text(50)),
        'semester_id' => \App\Models\Semester::all()->random(),
        'students_info' => $faker->text(100),
        'details' => $faker->text(100)
    ];
});
