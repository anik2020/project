<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Meeting;
use Faker\Generator as Faker;
use App\Models\Teacher;
use Carbon\Carbon;

$factory->define(Meeting::class, function (Faker $faker) {
    return [
        'teacher_id' => Teacher::all()->random(),
        'date' => Carbon::now()->addDays(rand(1, 5))->format('l'),
        'time' => Carbon::now()->addRealHour(rand(1, 5))->format('H:i:sA')
    ];
});
