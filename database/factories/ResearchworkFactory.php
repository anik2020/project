<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Researchwork;
use Faker\Generator as Faker;

$factory->define(Researchwork::class, function (Faker $faker) {
    return [
        'topic' => $faker->text(100),
        'description'=>$faker->text(100),
        'teacher_id' => $faker->randomElement([\App\Models\Teacher::all()->random()]),
        'group_id' => $faker->randomElement([\App\Models\Group::all()->random()]),
        'status' => $faker->randomElement([1,2,3])
    ];
});
