<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Assign;
use Faker\Generator as Faker;

$factory->define(Assign::class, function (Faker $faker) {
    return [
        'teacher_id' => \App\Models\Teacher::all()->random(),
        'group_id' => \App\Models\Group::all()->random(),
        'status' => $faker->randomElement([0,1])
    ];
});
