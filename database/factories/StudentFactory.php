<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Student;
use App\Models\Department;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

$factory->define(Student::class, function (Faker $faker) {
    return [
        'name' => $faker->firstNameMale . ' ' . $faker->lastName,
        'department_id' => Department::all()->random(),
        'email' => $faker->unique()->safeEmail,
        'group_id' => \App\Models\Group::all()->random()->group_id,
        'mobile' => $faker->phoneNumber,
        'password' => Hash::make('123'),
        'address' => $faker->address
    ];
});