<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Models\Department;
use App\Models\Teacher;

$factory->define(Teacher::class, function (Faker $faker) {
    return [
        'name' => $faker->firstNameMale . ' ' . $faker->lastName,
        'initial' => $faker->randomElement([
            'SSH', 'SSK','SSD','SPI','KKR','WWE','PPD'
        ]),
        'department_id' => Department::all()->random(),
        'email' => $faker->unique()->safeEmail,
        'mobile' => $faker->phoneNumber,
        'address' => $faker->address,
        'password' => Hash::make('123'),
        'is_supervisor' => $faker->randomElement([0, 1]),
        'specialization'  => $faker->randomElement([
            'Data Analysis', 'Web Development', 'Machine Learning',

        ])
    ];
});
