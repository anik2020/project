<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Notice;
use Faker\Generator as Faker;

$factory->define(Notice::class, function (Faker $faker) {
    return [
        'title' => ucfirst($faker->text(60)),
        'notice_category_id' => \App\Models\NoticeCategory::all()->random(),
        'description' => ucfirst($faker->text(100)),
        'attach' => $faker->text(100).$faker->randomElement(['.jpg','.pdf','.docx'])

    ];
});
