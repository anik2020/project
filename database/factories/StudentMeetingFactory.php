<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\StudentMeeting;
use App\Models\Teacher;
use App\Models\Student;
use App\Models\Meeting;
use Faker\Generator as Faker;

$factory->define(StudentMeeting::class, function (Faker $faker) {
    return [
        'student_id' => Student::all()->random(),
        'meeting_id' => Meeting::all()->random(),
        'status' => $faker->randomElement([0,1])
    ];
});
